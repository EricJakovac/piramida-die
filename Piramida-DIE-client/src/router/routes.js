
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PiramidaKaficiSystem/IndexPage.vue') }
    ]
  },

  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },

  {
    path: '/Kafici',
    component: () => import('layouts/PiramidaKaficiSystemLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PiramidaKaficiSystem/KaficiIndex.vue') }
    ]
  },

  {
    path: '/Restorani',
    component: () => import('layouts/PiramidaKaficiSystemLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PiramidaKaficiSystem/RestoraniIndex.vue') }
    ]
  },

  {
    path: '/FastFood',
    component: () => import('layouts/PiramidaKaficiSystemLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PiramidaKaficiSystem/FastFoodIndex.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/PiramidaKaficiSystem/ErrorNotFound.vue')
  }
]

export default routes
