import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyDjUp2RzKpC9VKncku7Y82d0bo89W9fGNk',
  authDomain: 'piramida-ef65b.firebaseapp.com',
  projectId: 'piramida-ef65b',
  storageBucket: 'piramida-ef65b.appspot.com',
  messagingSenderId: '805352794035',
  appId: '1:805352794035:web:ee2010a34b3c2ea5d40dc6',
  measurementId: 'G-SNMLZLYHW4'
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth(app)
const db = getFirestore(app)
const storage = getStorage(app)

export default ({ app: vueApp }) => {
  vueApp.provide('$auth', auth)
  vueApp.provide('$db', db)
  vueApp.provide('$storage', storage)
}
