const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


var admin = require("firebase-admin")

var serviceAccount = require("./Kljuc.json")

var models = require('./models.js')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)

});

const db = admin.firestore();

app.get('/kafic/filter', (request, response) => {
    let res = []
    if (typeof request.query.id === 'undefined') {
        var cRef = db.collection('PiramidaKafici').orderBy('Ime kafica')
        cRef.get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch(function (error) {
                return response.send(
                    "Error getting documents: " + error
                )
            })
    } else {
        var docRef =
            db.collection('PiramidaKafici').doc(request.query.id)
        docRef.get()
            .then((doc) => {
                if (typeof doc.data() !== 'undefined') {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    return response.send(document)
                } else {
                    return response.send(
                        "Error getting document " +
                        request.query.id +
                        ": The document is undefined"
                    )
                }
            })
            .catch(function (error) {
                return response.send(
                    "Error getting document " +
                    request.query.id +
                    ": " + error
                )
            })
    }
})

app.get('/kafic', async (request, response) => {
    let id = (
        typeof request.query.id !== 'undefined'
            ? request.query.id
            : null
    )
    let order = {
        orderAttr: (
            typeof request.query.orderAttr !== 'undefined'
                ? request.query.orderAttr
                : null
        ),
        orderType: (
            typeof request.query.orderDesc !== 'undefined'
                ? 'desc'
                : 'asc'
        )
    }
    models.get(db, 'PiramidaKafici', id, order)
        .then(res => {
            return response.send(res)
        }).catch((error) => {
            return response.send(error)
        })
})

app.get('/kafic', async (request, response) => {
    let id = (
        typeof request.query.id !== 'undefined'
            ? request.query.id
            : null
    )
    let order = null
    let where = null
    if (id === null) {
        order = models.getOrder(request.query)
        where = models.getWhere(request.query)
    }
    models.get(db, 'PiramidaKafici', id, order, where)
        .then(res => {
            return response.send(res)
        }).catch((error) => {
            return response.send(error)
        })
})
 
app.get('/kafic', async (request, response) => {
    if (typeof request.query.id === 'undefined') {
        let res = []
        db.collection('PiramidaKafici').get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch(function (error) {
                return response.send(
                    "Error getting documents: " + error
                )
            })
    } else {
        if (typeof request.query.subCollection === 'undefined') {
            let docRef = db
                .collection('PiramidaKafici')
                .doc(request.query.id)
            docRef.get()
                .then((doc) => {
                    if (typeof doc.data() !== 'undefined') {
                        let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        return response.send(document)
                    } else {
                        let error = (
                            "Error getting document " +
                            id +
                            ": The document is undefined"
                        )
                        return response.send(error)
                    }
                })
                .catch(function (error) {
                    return response.send(error)
                })
        } else {
            let res = []
            let cRef = db
                .collection('PiramidaKafici')
                .doc(request.query.id)
                .collection(request.query.subCollection)
            cRef.get()
                .then((querySnapshot) => {
                    querySnapshot.forEach((doc) => {
                        let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        res.push(document)
                    })
                    return response.send(res)
                })
                .catch(function (error) {
                    return response.send(
                        "Error getting documents " +
                        "of subcollection: " +
                        error
                    )
                })
        }
    }
})

app.post('/kafic', (request, response) => {
    if (Object.keys(request.body).length) {
        db.collection('PiramidaKafici').doc().set(request.body)
            .then(function () {
                return response.send(
                    "Document successfully written - created!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error writing document: " + error
                )
            })
    } else {
        return response.send(
            "No post data for new document. " +
            "A new document is not created!"
        )
    }
})

app.put('/kafic', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.query.id !== 'undefined') {
            db.collection('PiramidaKafici')
                .doc(request.query.id)
                .set(request.body)
                .then(function () {
                    return response.send(
                        "Document successfully written - " +
                        "updated!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " +
            "A document is not updated!"
        )
    }
})

app.delete('/kafic', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
        db.collection('PiramidaKafici').doc(request.query.id).delete()
            .then(function () {
                return response.send(
                    "Document successfully deleted!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error removing document: " + error
                )
            })
    } else {
        return response.send(
            "A parameter id is not set. " +
            "A document is not deleted!"
        )
    }
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
})